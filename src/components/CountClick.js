import { Component } from "react";



class CountClick extends Component{
    constructor(props){
        super(props);

        this.state = { 
            count: 0
        }
        // cach 1: sử dụng bind trỏ thí của function về class
        this.clickChangeHandler = this.clickChangeHandler.bind(this)
    }
    // cách 1
    // clickChangeHandler(){
    //     this.setState({
    //         count: this.state.count + 1
    //     })
    // }
// cách 2 sử dụng arrow function
// arrơ function ko có this. nên thí sẽ trỏ thẳng ra class luôn
    clickChangeHandler = () => {
        this.setState({
            count: this.state.count + 1
        })
    }
    render(){
        return(
            <div>
                <p>Count: {this.state.count}</p>
                <button onClick={this.clickChangeHandler}>Cluck here</button>
            </div>
        )
    }
}
export default CountClick